import sqlite3
from tkinter.ttk import Combobox
from PyQt5.QtWidgets import QComboBox
from kivy.app import App
from kivy.lang import Builder
from kivy.metrics import dp, cm
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.recycleview import RecycleView
from kivy.uix.textinput import TextInput

Builder.load_string("""

<Table>
    orientation:'horizontal'
    column1:'column1'
    column2:'column2'
    column3:'column3'
    Label:
        id:column1
        text:root.column1
        size_hint_x:0
    Label:
        id:column2
        text:root.column2
        size_hint_x:0.2
    Label:
        id:column3
        text:root.column3

<RV>
    viewclass :'Table'
    RecycleBoxLayout:
        id:rv_data_list
        default_size:None,dp(20)
        default_size_hint:1,None
        size_hint_y:None
        height:self.minimum_height
        orientation:'vertical'

""")


class Table(BoxLayout):
    pass

class RV(RecycleView):
    def __init__(self, **kwargs):
        super(RV,self).__init__(**kwargs)
        con = sqlite3.connect(r'C:\SQLite\table.sqlite')
        cur=con.cursor()
        cur.execute("select * from sheet")
        rows=cur.fetchall()
        self.data=[{'column1':str(row[0]),'column2':str(row[1]),'column3':str(row[2])} for row in rows]

class MyTable(App):
    def clrinfo(self, *largs):
        con = sqlite3.connect(r'C:\SQLite\table.sqlite')
        cur = con.cursor()
        cur.execute("select * from sheet")
        rows = cur.fetchall()
        self.rv.data = [{'column1': str(row[0]), 'column2': str(row[1]), 'column3': str(row[2])} for row in rows]
    def searchinfo(self, *largs):
        if self.ti1.text!="":
            con = sqlite3.connect(r'C:\SQLite\table.sqlite')
            cur = con.cursor()
            selq = 'select * from sheet where id=' + str(self.ti1.text)+';';
            cur.execute(selq)
            rows = cur.fetchall()
            self.rv.data=[{'column1':str(row[0]),'column2':str(row[1]),'column3':str(row[2])} for row in rows]
    def searchinfo2(self, *largs):
        if self.ti2.text != "":
            con = sqlite3.connect(r'C:\SQLite\table.sqlite')
            cur = con.cursor()
            selq = 'select * from sheet' +  " where date='" + str(self.ti2.text) + "';"
            cur.execute(selq)
            rows = cur.fetchall()
            self.rv.data=[{'column1':str(row[0]),'column2':str(row[1]),'column3':str(row[2])} for row in rows]
    def searchinfo3(self, *largs):
        if self.ti3.text != "":
            con = sqlite3.connect(r'C:\SQLite\table.sqlite')
            cur = con.cursor()
            selq = 'select * from sheet' +  " where string='" + str(self.ti3.text) + "';"
            cur.execute(selq)
            rows = cur.fetchall()
            self.rv.data=[{'column1':str(row[0]),'column2':str(row[1]),'column3':str(row[2])} for row in rows]
    def build(self):
        gl=GridLayout(rows=5)
        bl1=BoxLayout(orientation='horizontal',size_hint_y=None, height=dp(50))
        bl1.add_widget(Label(text='Ид',size_hint_x=0,font_size='30sp'))
        bl1.add_widget(Label(text='Дата',size_hint_x=0.2,font_size='30sp'))
        bl1.add_widget(Label(text='Название',font_size='30sp'))
        bl2 = BoxLayout(orientation='horizontal')
        bl3 = BoxLayout(orientation='horizontal',size_hint_y=None, height=dp(50))
        self.ti1=TextInput(size_hint_x=.1)
        bl3.add_widget(self.ti1)
        self.ti2 =TextInput(size_hint_x=.3)
        bl3.add_widget(self.ti2)
        self.ti3=TextInput(size_hint_x=.5)
        bl3.add_widget(self.ti3)
        bl4 = BoxLayout(orientation='horizontal', size_hint_y=None, height=dp(50))
        btn1=Button(text='Найти ид',size_hint_x=.1)
        btn1.bind(on_release=self.searchinfo)
        bl4.add_widget(btn1)
        btn2=Button(text='Найти дату',size_hint_x=.3)
        btn2.bind(on_release=self.searchinfo2)
        bl4.add_widget(btn2)
        btn3 = Button(text='Найти название', size_hint_x=.5)
        btn3.bind(on_release=self.searchinfo3)
        bl4.add_widget(btn3)
        self.rv=RV(bar_width='10sp')
        bl2.add_widget(self.rv)
        gl.add_widget(bl1)
        gl.add_widget(bl2)
        gl.add_widget(bl3)
        gl.add_widget(bl4)
        btnclr=Button(text="Сбросить",size_hint_y=None, height=dp(50))
        gl.add_widget(btnclr)
        btnclr.bind(on_release=self.clrinfo)
        return gl

MyTable().run()

'''
workbook = xlrd.open_workbook(r'D:\Downloads\table.xlsx')
worksheet = workbook.sheet_by_name('Sheet1')

con = sqlite3.connect(r'C:\SQLite\table.sqlite')
query = "create table if not exists sheet (id integer primary key, date text, string text);"
con.execute(query)

for i in range(1, worksheet.nrows):
    insquery = 'insert into sheet (id,date,string) values(' + str(worksheet.cell_value(i, 1)) + ", '" + str(
        worksheet.cell_value(i, 4)) + "', '" + str(worksheet.cell_value(i, 6)) + "')";
    con.execute(insquery)
    con.commit()
con.close()
'''
